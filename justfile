default:
	just --choose

# Build all the rust
build:
	cargo fmt --all -- --check
	cargo build --release --workspace

# Build (debug) all the rust
debug:
	cargo build --workspace

# Type-check and lint the code
clippy:
	cargo fmt --all -- --check
	cargo clippy --workspace

# Run Rust tests
test:
	cargo nextest run --workspace

# Clean everything
clean:
	cargo clean
