use std::io;

use clap::{Parser, Subcommand};

mod cmd_cat;
mod cmd_dump_path;
mod cmd_ls;

#[cfg(test)]
mod cmd_cat_test;
#[cfg(test)]
mod cmd_dump_path_test;
#[cfg(test)]
mod cmd_ls_test;

/// Create or inspect NAR files
#[derive(Parser)]
#[clap(author, version, about)]
struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    /// Print the contents of a file inside a NAR file on stdout
    Cat(cmd_cat::Opts),

    /// Serialise a path to stdout in NAR format
    DumpPath(cmd_dump_path::Opts),

    /// Show information about a path inside a NAR file
    Ls(cmd_ls::Opts),
}

pub fn main() -> Result<(), anyhow::Error> {
    let opts = Opts::parse();
    match opts.cmd {
        Cmd::Cat(o) => cmd_cat::run(io::stdout(), o)?,
        Cmd::DumpPath(o) => cmd_dump_path::run(io::stdout(), o)?,
        Cmd::Ls(o) => cmd_ls::run(io::stdout(), o)?,
    }
    Ok(())
}

#[cfg(test)]
mod test_setup {
    use std::fs;

    #[ctor::ctor]
    fn setup() {
        fs::create_dir_all("test-data/01-empty-dir.in").unwrap();
        if let Err(_) = fs::symlink_metadata("test-data/06-symlink.in") {
            symlink::symlink_file("02-empty-file.in", "test-data/06-symlink.in").unwrap();
        }
        if let Err(_) = fs::symlink_metadata(
            "test-data/07-nested-dirs.in/02-some-dir/link-to-an-empty-file",
        ) {
            symlink::symlink_file(
                "../01-an-empty-file",
                "test-data/07-nested-dirs.in/02-some-dir/link-to-an-empty-file",
            )
            .unwrap();
        }
    }
}
