#!/bin/sh

set -e -x

for f in *.in *.in.exe; do
    nix nar dump-path "$f" > "${f%.in}.nar"
done
