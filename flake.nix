{

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nmattia/naersk/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.flake-utils.follows = "utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-compat = {
      url = github:edolstra/flake-compat;
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, rust-overlay, naersk, ... }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        lib = pkgs.lib;
        stdenv = pkgs.stdenv;
        naersk-lib = pkgs.callPackage naersk { };
        on-x64-linux = system == utils.lib.system.x86_64-linux;

        pkgs-musl = (nixpkgs.legacyPackages.${system}.extend rust-overlay.overlays.default);
        rust-musl = (pkgs-musl.rust-bin.stable.latest.default.override {
          extensions = [ "rust-src" "rust-analyzer"];
          targets = [ "x86_64-unknown-linux-musl" ];
        });
        naersk-lib-musl = pkgs-musl.callPackage naersk { cargo = rust-musl; rustc = rust-musl; };
        # On darwin, we need a cross-compiled linux GCC linker, otherwise the
        # static binary fails to build with `ld: unknown option: --as-needed`.
        # The rest of the build is still done with native clang, though.
        cross-gcc = pkgs-musl.pkgsCross.musl64.stdenv.cc;
        cross-linker = "${cross-gcc}/bin/${cross-gcc.targetPrefix}ld";
      in
      {
        packages= {
          default = naersk-lib.buildPackage {
            src = ./.;
            cargoBuildOptions = x: x ++ [ "-p" "nix-nar-cli" ];
            cargoTestOptions = x: x ++ [ "-p" "nix-nar-cli" ];
            doCheck = true;
            pname = "nix-nar";
          };

          static-x86_64-linux = naersk-lib-musl.buildPackage ({
            src = ./.;
            cargoBuildOptions = x: x ++ [ "-p" "nix-nar-cli" "--target=x86_64-unknown-linux-musl" ];
            cargoTestOptions = x: x ++ [ "-p" "nix-nar-cli" "--target=x86_64-unknown-linux-musl" ];
            doCheck = on-x64-linux;
            pname = "nix-nar-static-x86_64-linux";
          } // lib.optionalAttrs (stdenv.hostPlatform.isDarwin) {
            CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_LINKER = cross-linker;
          });
        };

        apps = {
          default = utils.lib.mkApp {
            drv = self.packages.${system}.default;
          };
        } // lib.optionalAttrs (on-x64-linux) {
          static = utils.lib.mkApp {
            drv = self.packages.x86_64-linux.static-x86_64-linux;
          };
        };

        devShell = with pkgs; mkShell {
          buildInputs = [
            cargo
            cargo-insta
            cargo-nextest
            cargo-outdated
            difftastic
            just
            pre-commit
            rust-analyzer
            rustPackages.clippy
            rustc
            rustfmt
            tokei
          ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });
}
