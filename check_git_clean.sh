#!/bin/bash

set -e

if [[ -z "$(git status --porcelain)" ]]; then
    true
else
    echo "Unclean working directory"
    git status --porcelain
    exit 1
fi
