use thiserror::Error;

#[derive(Debug, Error, Eq, PartialEq)]
#[non_exhaustive]
pub enum NarError {
    #[error("IO error: {0}")]
    IoError(String),
    #[error("Parse error: {0}")]
    ParseError(String),
    #[error("API error: {0}")]
    ApiError(String),
    #[error("Pack error: {0}")]
    PackError(String),
    #[error("Unpack error: {0}")]
    UnpackError(String),
    #[error("UTF8 path error: {0}")]
    Utf8PathError(String),
}

impl From<std::io::Error> for NarError {
    fn from(v: std::io::Error) -> Self {
        Self::IoError(v.to_string())
    }
}
