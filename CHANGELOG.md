0.3.0
=====

This release adds partial Windows support.

### BREAKING CHANGES:
  - `Encoder::new` and `EncoderBuilder::build` are now failable and
    return `Result<_, _>`,
  - `Entry::path` and `Content::Symlink::target` are now
    `camino::Utf8PathBuf` instead of `std::file::PathBuf`.

### New features:
  - Partial Windows support ([MR !10](https://gitlab.com/abstract-binary/nix-nar-rs/-/merge_requests/10)).

### Changes:
  - `NarError` has a new variant: `Utf8PathError(String)`.

Thanks @mrman!


0.2.1
=====

This release just updates dependencies to the latest versions.

### BREAKING CHANGES:
  - None

### New features:
  - None
  
### Changes:
  - None


0.2.0
=====

This release hides the modules, re-exports the data structures, and
improves docs all around.

### BREAKING CHANGES:
  - Reworked module hierarchy such that modules are not public, but
    their structs are public.

### New features:
  - Add `Encoder::pack`,
  - Add `Decoder::unpack`,
  - Add `EncoderBuilder` to set encoder options,
  - Add `debug::pretty_print_nar_content`.
  
### Changes:
  - Remove the 128 byte requirement on `Encoder::read`'s buffer.  The
    requirement is now on `Encoder`'s internal buffer, which defaults
    to 1024 bytes.
